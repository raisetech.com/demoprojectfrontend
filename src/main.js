// Import ES6 Promise
import 'es6-promise/auto'

// Import System requirements
import Vue from 'vue'
import VueRouter from 'vue-router'

import { sync } from 'vuex-router-sync'
import routes from './routes'
import store from './store'
import 'vue2-datepicker/index.css'
import ToastService from 'primevue/toastservice'
import 'primevue/resources/primevue.min.css'
import 'primevue/resources/themes/nova-light/theme.css'
import 'primeicons/primeicons.css'
import Tooltip from 'primevue/tooltip'
import 'primeflex/primeflex.css'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.directive('tooltip', Tooltip)

// Import Views - Top level
import AppView from './components/App.vue'
Vue.use(ToastService)
Vue.use(VueRouter)
// Routing logic
var router = new VueRouter({
  routes: routes,
  mode: 'history',
  linkExactActiveClass: 'active',
  scrollBehavior: function(to, from, savedPosition) {
    return savedPosition || { x: 0, y: 0 }
  }
})

// Some middleware to help us ensure the user is authenticated.
router.beforeEach((to, from, next) => {
  if (
    to.matched.some(record => record.meta.requiresAuth) &&
    (!router.app.$store.state.access_token || router.app.$store.state.access_token === 'null' || router.app.$store.state.expiry < Date.now())
  ) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    next({
      path: '/login',
      query: { redirect: to.fullPath }
    })
  } else {
    next()
  }
})

sync(store, router)

// Check local storage to handle refreshes
if (window.localStorage) {
  let localUserString = window.localStorage.getItem('user') || 'null'
  let localUser = JSON.parse(localUserString)
  let localWarehouse = JSON.parse(window.localStorage.getItem('getWarehouse'))
  let localCounter = JSON.parse(window.localStorage.getItem('getCounter'))

  if (localUser && store.state.user !== localUser) {
    store.commit('SET_USER', localUser)
    store.commit('SET_TOKEN', window.localStorage.getItem('token'))
    store.commit('ORGANISATIONID', window.localStorage.getItem('organisationId'))
    store.commit('STARTDATE', window.localStorage.getItem('startDate'))
    store.commit('ENDDATE', window.localStorage.getItem('endDate'))
    store.commit('FYID', window.localStorage.getItem('fyId'))
    store.commit('EXPIRY', window.localStorage.getItem('expiry'))
    store.commit('MODULES', window.localStorage.getItem('modules'))
    store.commit('NEPALI_NEWDATE', window.localStorage.getItem('nepaliNewDate'))
    store.commit('ENGLISHDATEENABLED', window.localStorage.getItem('englishDate'))
    store.commit('SET_WAREHOUSE_DATA', localWarehouse)
    store.commit('SET_COUNTER_DATA', localCounter)
    store.commit('FYNAME', window.localStorage.getItem('fyName'))
    store.commit('ORGANISATIONAME', window.localStorage.getItem('organisationName'))
    store.commit('ORGANISATIONADDRESS', window.localStorage.getItem('organisationAddress'))
    store.commit('ORGANISATIONCONTACT', window.localStorage.getItem('organisationContact'))
    store.commit('ORGANISATIONLOGO', window.localStorage.getItem('organisationLogo'))
    store.commit('ORGANISATIONPANVATNO', window.localStorage.getItem('organisationPanVatNo'))
    store.commit('AGENTID', window.localStorage.getItem('agentId'))
  }
}

Vue.config.productionTip = false

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyC2yEjw1xE4Rtn0MbVk-BzjMYIi1eFuZwQ',
    libraries: 'places,drawing,visualization,geometry',
    mapTypeId: 'google.maps.MapTypeId.SATELLITE'
  }
})

// Start out app!
// eslint-disable-next-line no-new
new Vue({
  el: '#root',
  router: router,
  store: store,
  created() {
  },
  render: h => h(AppView)
})

// window.bugsnagClient = window.bugsnag('02fe1c2caaf5874c50b6ee19534f5932')
// window.bugsnagClient.use(window.bugsnag__vue(Vue))
