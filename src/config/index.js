import globalKey from '../store/globalKey'

export default {
  serverURI: globalKey,
  fixedLayout: true,
  hideLogoOnMobile: false
}
