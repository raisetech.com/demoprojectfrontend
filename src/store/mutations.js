export default {
  TOGGLE_LOADING (state) {
    state.callingAPI = !state.callingAPI
  },
  SET_USER (state, user) {
    state.user = user
  },
  SET_TOKEN (state, token) {
    state.access_token = token
  },
  ORGANISATIONID (state, organisation) {
    state.organisationid = organisation
  },
  STARTDATE (state, startDate) {
    state.startDate = startDate
  },
  ENGLISHDATEENABLED (state, englishDate) {
    state.isEnglishDateEnabled = englishDate
  },
  ENDDATE (state, endDate) {
    state.endDate = endDate
  },
  TRANSACTIONDATE (state, transactionDate) {
    state.transactionDate = transactionDate
  },
  FYID (state, fyId) {
    state.fyId = fyId
  },
  EXPIRY (state, expiry) {
    state.expiry = expiry
  },
  MODULES(state, modules) {
    state.modules = modules
  },
  NEPALI_NEWDATE(state, nepaliDate) {
    state.nepaliNewDate = nepaliDate
  },
  SET_WAREHOUSE_DATA (state, warehouse) {
    state.warehouseData = warehouse
  },
  SET_COUNTER_DATA (state, counter) {
    state.counterData = counter
  },
  FYNAME (state, fyName) {
    state.fyName = fyName
  },
  ORGANISATIONAME(state, organisationName) {
    state.organisationName = organisationName
  },
  ORGANISATIONADDRESS(state, organisationAddress) {
    state.organisationAddress = organisationAddress
  },
  ORGANISATIONCONTACT(state, organisationContact) {
    state.organisationContact = organisationContact
  },
  ORGANISATIONLOGO(state, organisationLogo) {
    state.organisationLogo = organisationLogo
  },
  ORGANISATIONPANVATNO(state, organisationPanVatNo) {
    state.organisationPanVatNo = organisationPanVatNo
  },
  AGENTID(state, agnetId) {
    state.agnetId = agnetId
  },
  CUSTOMERID(state, customerId) {
    state.customerId = customerId
  }
}
