const getters = {
  organisationId: state => {
    return state.organisationid
  },
  modules: state => {
    return state.modules
  },
  userId: state => {
    return state.user.id
  },
  englishDate: state => {
    return state.isEnglishDateEnabled
  },
  nepaliNewDate: state => {
    return state.nepaliNewDate
  },
  fyId: state => {
    return state.fyId
  },
  startDate: state => {
    return state.startDate
  },
  endDate: state => {
    return state.endDate
  },
  fyName: state => {
    return state.fyName
  },
  organisationLogo: state => {
    return state.organisationLogo
  },
  organisationPanVatNo: state => {
    return state.organisationPanVatNo
  },
  agentId: state => {
    return state.agentId
  },
  customerId: state => {
    return state.customerId
  }
}
export default getters
