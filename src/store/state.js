export default {
  callingAPI: false,
  access_token: null,
  organisationid: '',
  customerId: '',
  isEnglishDateEnabled: false,
  modules: '',
  endDate: '',
  transactionDate: '',
  expiry: '',
  fyId: '',
  startDate: '',
  nepaliNewDate: '',
  fyName: '',
  user: {
  },
  organisationName: '',
  organisationAddress: '',
  organisationContact: '',
  organisationLogo: '',
  organisationPanVatNo: '',
  agentId: ''
}
