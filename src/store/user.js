import Api from '../api'
import store from '../store'

export default ({

  state: {
    userLists: [],
    roleLists: [],
    moduleList: [],
    basicAccountList: []
  },
  mutations: {
    ADD_USER (state, userLists) {
      let userList = state.userLists.concat(userLists)
      state.userLists = userList
    },
    SET_USERS (state, userList) {
      state.userLists = userList
    },
    ADD_ROLE (state, roleLists) {
      let roleList = state.roleLists.concat(roleLists)
      state.roleLists = roleList
    },
    SET_ROLE (state, roleList) {
      state.roleLists = roleList
    },
    DELETE_USER (state, userId) {
      let user = state.userLists.filter(a => a.id !== userId.id)
      state.userLists = user
    },
    DELETE_ROLE (state, roleId) {
      let role = state.roleLists.filter(a => a.id !== roleId.id)
      state.roleLists = role
    },
    UPDATE_USER(state, updatedUser) {
      state.userLists = state.userLists.map(user => user.id !== updatedUser.id ? user : {...user, ...updatedUser})
    },
    UPDATE_ROLE(state, updatedRole) {
      state.roleLists = state.roleLists.map(role => role.id !== updatedRole.id ? role : {...role, ...updatedRole})
      store.commit('MODULES', updatedRole.modules)
    },
    SET_MODULE(state, module) {
      state.moduleList = module
    },
    SET_BASICACCOUNT_LIST(state, basicAccounts) {
      state.basicAccountList = basicAccounts
    },
    UPDATE_BASICACCOUNT_SETUP(state, updatedBasicAccount) {
      state.basicAccountList = state.basicAccountList.map(basicAccount => basicAccount.id !== updatedBasicAccount.id ? basicAccount : {...basicAccount, ...updatedBasicAccount})
    }
  },
  actions: {
    async createUser({commit}, user) {
      let response = await Api.request('post', '/users/' + this.state.user.id, user)
      let savedUser = response.data.data
      if (response.data.status === 'SUCCESS') {
        commit('ADD_USER', savedUser)
      }
      return response
    },
    async loadUser({commit}) {
      let response = await Api.request('get', '/users/userList', null)
      commit('SET_USERS', response.data.data)
      return response
    },
    async createRole({commit}, role) {
      let response = await Api.request('post', '/role/' + this.state.user.id, role)
      let savedRole = response.data.data
      if (response.data.status === 'SUCCESS') {
        commit('ADD_ROLE', savedRole)
      }
      return response
    },
    async updateRole({commit}, role) {
      let response = await Api.request('put', '/role/' + role.id, role)
      if (response.data.status === 'SUCCESS') {
        commit('UPDATE_ROLE', response.data.data)
      }
      return response
    },
    async loadRole({commit}) {
      let response = await Api.request('get', '/role', null)
      commit('SET_ROLE', response.data.data)
    },
    async deleteUser({commit}, user) {
      let response = await Api.request('delete', '/users/' + user.userId + '/' + this.state.user.id + '/' + user.deleteReason)
      let userLists = response.data.data
      if (response.data.status === 'SUCCESS') {
        commit('DELETE_USER', userLists)
      }
      return response
    },
    async deleteRole({commit}, role) {
      let response = await Api.request('delete', '/role/' + role.roleId + '/' + this.state.user.id + '/' + role.deleteReason)
      let roleLists = response.data.data
      if (response.data.status === 'SUCCESS') {
        commit('DELETE_ROLE', roleLists)
      }
      return response
    },
    async updateUser({commit}, user) {
      let response = await Api.request('put', '/users/' + user.userId + '/' + this.state.user.id, user)
      commit('UPDATE_USER', response.data.data)
      return response
    },
    async updatePassword({commit}, passwordDetail) {
      let response = await Api.request('put', '/users/resetPassword/' + this.state.user.id, passwordDetail)
      return response
    },
    async moduleList({commit}) {
      let response = await Api.request('get', '/role/moduleList')
      commit('SET_MODULE', response.data.data)
      return response
    },
    async sendInquiry({commit}, inquiryData) {
      let response = await Api.request('post', '/inquiry', inquiryData)
      return response
    },
    async loadDailyTransactionSummary({commit}) {
      let response = await Api.request('get', '/dashboard/dailyTransactionSummary/' + this.state.organisationid + '/' + this.state.fyId + '/' + this.state.nepaliNewDate)
      if (response.data.status === 'SUCCESS') {
        return response
      }
    },
    async loadDailyCashSummary({commit}) {
      let response = await Api.request('get', '/dashboard/dailyCashSummary/' + this.state.organisationid + '/' + this.state.fyId + '/' + this.state.nepaliNewDate)
      if (response.data.status === 'SUCCESS') {
        return response
      }
    },
    async basicAccountSetupList({commit}) {
      let response = await Api.request('get', '/basicAccount/' + this.state.organisationid)
      commit('SET_BASICACCOUNT_LIST', response.data.data)
      return response
    },
    async updateBasicAccountSetUp({commit}, data) {
      let response = await Api.request('put', '/basicAccount/' + data.id + '/' + data.accountMasterId)
      commit('UPDATE_BASICACCOUNT_SETUP', response.data.data)
      return response
    },
    async loadTotalCounts({commit}) {
      let response = await Api.request('get', '/report/dashboardCounts/' + this.state.user.id)
      if (response.data.status === 'SUCCESS') {
        return response
      }
    }
  },
  getters: {
  }
})
