import DashView from './components/Dash.vue'
import LoginView from './components/Login.vue'
import NotFoundView from './components/404.vue'
import Inquiry from './components/inquiry.vue'
// import Router from 'vue-router'

import DashboardView from './components/views/Dashboard.vue'

// Settings
import user from './components/views/settings/user.vue'
// Routes
const routes = [
  {
    name: 'Login',
    path: '/login',
    component: LoginView
  },
  {
    path: '/inquiry',
    component: Inquiry
  },
  {
    path: '/',
    component: DashView,
    children: [
      {
        path: 'dashboard',
        alias: '',
        component: DashboardView,
        name: 'Dashboard',
        meta: { requiresAuth: true }
      },
      // Admin Module Router
      {
        path: 'user',
        component: user,
        name: 'User',
        meta: { requiresAuth: true }
      }
    ]
  }, {
    // not found handler
    path: '/**',
    component: NotFoundView
  }
]

export default routes
